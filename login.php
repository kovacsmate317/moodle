<?php
session_start();
//$_SESSION["szerepkor"] = "";
$letezik = true;
$helyesjelszo = true;
$sikeres = false;
if(isset($_POST['submit'])) {
    $felhasznalonev = $_POST['felhasznalonev'];
    $jelszo = trim($_POST['jelszo']);

    $sqlid = mysqli_connect('localhost', 'root', '', 'moodle') or die("Hibás csatlakozás!");


    mysqli_query($sqlid, "SET character_set_results=utf8");
    mysqli_set_charset($sqlid, 'utf8');

    $eredmeny = "";
    if (mysqli_select_db($sqlid, 'moodle')) {
        $lekerd = "SELECT felhasználónév,jelszó,szerepkör FROM `felhasználó` WHERE `felhasználónév` LIKE '$felhasznalonev'";

        $eredmeny = mysqli_query($sqlid, $lekerd) or die("hiba");
    } else {
        echo "hiba";
    }
    $row = mysqli_fetch_assoc($eredmeny);
    if(!empty($row)){
        if($row["felhasználónév"] === $felhasznalonev && password_verify($jelszo, $row["jelszó"])) {
            $_SESSION["felhasznalonev"] = $felhasznalonev;
            $_SESSION["szerepkor"] = $row["szerepkör"];
            $sikeres = true;

            $updateQuery = "UPDATE felhasználó SET bejelentkezve = 1 WHERE felhasználónév = ?";
            $updateStmt = mysqli_prepare($sqlid, $updateQuery);
            mysqli_stmt_bind_param($updateStmt, "s", $felhasznalonev);
            mysqli_stmt_execute($updateStmt);
            mysqli_stmt_close($updateStmt);

            header("Location: kurzusok.php");
            exit;
        }else{
            $helyesjelszo = false;
        }
    }else{
        $letezik = false;
    }
    mysqli_close($sqlid);
}
?>
<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Bejelentkezés</title>
    <link rel="stylesheet" href="css.css">
</head>
<body>
<div id="navdiv">
    <nav>
        <?php if(!isset($_SESSION["felhasznalonev"]) || empty($_SESSION["felhasznalonev"])):?>
        <a href="login.php" class="navlink" id="active">Bejelentkezés</a>
        <?php endif; ?>
        <?php if(isset($_SESSION["felhasznalonev"])):
            header("Location: kurzusok.php");
            exit;
            ?>

        <?php endif; ?>
    </nav>
</div>

<main>
<div id="logindiv">
        <form action="login.php" method="POST" autocomplete="off" enctype="multipart/form-data">
    <fieldset>
        <legend style="text-align: center;">Bejelentkezés</legend>
                <label>Felhasználónév
				<br>
                    <input type="text" class="loginwidth" name="felhasznalonev" maxlength="30" required>
                </label>
				<br>
                <label>Jelszó
				<br>
                    <input type="password" class="loginwidth" name="jelszo" maxlength="50" required >
					<br>
                </label>
                   <input type="submit" name="submit" value="Küldés" class="formgombok">
    </fieldset>
        </form>
		<a href="regisztracio.php" id="nincsmegfiokod">Nincs még fiókod?</a>
    <?php
        if(!$helyesjelszo){
            echo "Helytelen jelszó<br>";
        }
    if(!$letezik){
        echo "Nem létezik ilyen felhasználó<br>";
    }
    if($sikeres){
        echo "Sikeres login";
    }
    ?>
</div>
</main>
<div></div>

<footer>
    <p style="text-align: center; font-family: Arial,sans-serif; font-size: 12px; height: 10px"></p>
</footer>

</body>
</html>
<?php
 session_start();
    $eredmeny = "";
    $sqlid = mysqli_connect('localhost', 'root', '', 'moodle') or die("Hibás csatlakozás!");
    $kurzusid = $_SESSION['kkurzusid'];
    $tkurzusid = $_GET['tkurzusid'];
    $_SESSION["tkurzusid"] = $tkurzusid;

    mysqli_query($sqlid, "SET character_set_results=utf8");
    mysqli_set_charset($sqlid, 'utf8');
if (mysqli_select_db($sqlid, 'moodle')) {
    $lekerd = "SELECT szöveg FROM `tananyag` WHERE `tananyag azonosító` = '$tkurzusid' AND `kurzuskód` = '$kurzusid'";
    $eredmeny = mysqli_query($sqlid, $lekerd) or die("hiba");



    if(isset($_GET["clicked"])){
        $insertIntnaplo = "INSERT INTO `napló` (`naplóid`, `mit`, `felhasználónév`,`mikor`, `kurzuskód`, `tananyag azonosító`, `eltelt idő`, `megnyitva`) VALUES (NULL,?, ?, CURRENT_TIMESTAMP, ?, ?, NULL, 'Igen'); ";
        $stmt = mysqli_prepare($sqlid, $insertIntnaplo);
        $mit = "Megnyitotta a " . $tkurzusid. " azonosítójú tananyagot";
        mysqli_stmt_bind_param($stmt, "ssii",  $mit, $_SESSION["felhasznalonev"] ,$kurzusid,$tkurzusid);

        if (mysqli_stmt_execute($stmt)) {
            $insertsiker = true;
            $indexhiba = false;

        } else {
            // Error
            echo "Error: " . mysqli_error($sqlid);
        }
    }
} else {
    echo "hiba";
}

?>


<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Tananyag szöveg</title>
    <link rel="stylesheet" href="css.css">
    <script>
        function showPage(pageNumber) {
            const pages = document.querySelectorAll('.page');
            const paginationLinks = document.querySelectorAll('.page-link');

            pages.forEach(page => page.classList.remove('active'));
            paginationLinks.forEach(link => link.classList.remove('active'));

            pages[pageNumber - 1].classList.add('active');
            paginationLinks[pageNumber - 1].classList.add('active');
        }
    </script>
    <style>
        #content {
            max-width: 600px;
            margin: 60px auto;
            padding: 20px;
        }
        .page {
            display: none;
        }
        .active {
            display: block;
        }
        .pagination {
            text-align: center;
            margin-top: 20px;
        }
        .page-link {
            cursor: pointer;
            padding: 5px 10px;
            margin: 0 5px;
            background-color: black;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            width: 30px; /* Set a fixed width */
            display: inline-block; /* Make sure they are inline elements */
            text-align: center;
        }
    </style>
</head>
<body>

<div id="navdiv">

    <nav>
        <?php if(!isset($_SESSION["felhasznalonev"]) || empty($_SESSION["felhasznalonev"])):;?>
            <a href="login.php" class="navlink">Bejelentkezés</a>
            <?php header("Location: login.php"); endif; ?>
        <?php if(isset($_SESSION["felhasznalonev"]) && ($_SESSION["szerepkor"] == "admin" || $_SESSION["szerepkor"] == "tanár")):?>
            <a href="kurzusok.php" class="navlink">Kurzusok</a>
            <a href="naplo.php" class="navlink">Napló</a>
        <?php elseif(isset($_SESSION["felhasznalonev"]) && $_SESSION["szerepkor"] == "diák"):?>
            <a href="kurzusok.php" class="navlink">Kurzusok</a>
        <?php endif; ?>
    </nav>
</div>

<main>
    <div id="content">
        <?php
        if (mysqli_num_rows($eredmeny) > 0) {
            $egySor = mysqli_fetch_row($eredmeny);
            $szoveg = $egySor[0];
            $words = explode(" ", $szoveg);
            $wordsPerPage = 200;
            $pages = array_chunk($words, $wordsPerPage);
            ?>
            <div id="page1" class="page active">
                <?php echo implode(" ", $pages[0]); ?>
            </div>
            <?php
            for ($i = 1; $i < count($pages); $i++) {
                ?>
                <div id="page<?php echo $i + 1; ?>" class="page">
                    <?php echo implode(" ", $pages[$i]); ?>
                </div>
                <?php
            }
            ?>
            <div class="pagination" id="pagination">
                <?php
                for ($i = 1; $i <= count($pages); $i++) {
                    ?>
                    <a href="#" class="page-link" onclick="showPage(<?php echo $i; ?>)"><?php echo $i; ?></a>
                    <?php
                }
                ?>
                <form action='kurzusok.php?tkurzusid=<?php echo $tkurzusid; ?>' method="post" enctype="multipart/form-data"> <input type="submit" name="bezár" value="bezár" class="page-link" style="font-size: 16px; width: 75px;"> </form>
            </div>

            <?php
        } else {
            echo "No data found";
        }
        ?>
    </div>
</main>

<footer>
    <?php
    if(isset($_SESSION["felhasznalonev"])):?>)
        <p style="text-align: right; font-family: Arial,sans-serif; font-size: 12px;"><a style="color: white" href="kijelentkezes.php">Kijelentkezés</a></p>
    <?php endif; ?>
</footer>
</body>
</html>


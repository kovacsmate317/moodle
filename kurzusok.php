<?php
session_start();
$indexhiba = false;
$insertsiker = false;
$megnyitva = true;
$sqlid = mysqli_connect('localhost', 'root', '', 'moodle') or die("Hibás csatlakozás!");

mysqli_query($sqlid, "SET character_set_results=utf8");
mysqli_set_charset($sqlid, 'utf8');

if (mysqli_select_db($sqlid, 'moodle')) {
    $lekerd = "SELECT kurzus.*, 
                     (SELECT COUNT(*) FROM tananyag WHERE tananyag.kurzuskód = kurzus.kurzuskód) AS tananyag_száma
              FROM kurzus";
    $eredmeny = mysqli_query($sqlid, $lekerd) or die("hiba");
} else {
    echo "hiba" . mysqli_error($sqlid);
}

//ez nemjo
if(isset($_POST["bezár"])){

    $insertIntnaplo = "INSERT INTO `napló` (`naplóid`, `mit`, `felhasználónév`,`mikor`, `kurzuskód`, `tananyag azonosító`, `eltelt idő`) VALUES (NULL,?, ?, CURRENT_TIMESTAMP, ?, ?, NULL); ";
    $stmt = mysqli_prepare($sqlid, $insertIntnaplo);
    $mit = "Bezárta a " . $_SESSION["tkurzusid"] . " azonosítójú tananyagot";
    mysqli_stmt_bind_param($stmt, "ssii",  $mit, $_SESSION["felhasznalonev"] ,$_SESSION["kkurzusid"],$_SESSION["tkurzusid"]);

    if (mysqli_stmt_execute($stmt)) {
        $insertsiker = true;
        $indexhiba = false;

    } else {
        echo "Error: " . mysqli_error($sqlid);
    }
}

if (isset($_GET["delete"]) && ($_SESSION["szerepkor"] === "admin" || $_SESSION["szerepkor"] === "tanár")) {
    $deleteId = $_GET["delete"];
    $deleteQuery = "DELETE kurzus, napló FROM kurzus
                   LEFT JOIN napló ON kurzus.kurzuskód = napló.kurzuskód
                   WHERE kurzus.kurzuskód = ?";
    $deleteStmt = mysqli_prepare($sqlid, $deleteQuery);
    mysqli_stmt_bind_param($deleteStmt, "i", $deleteId);

    if (mysqli_stmt_execute($deleteStmt)) {
       header("Location: kurzusok.php");
        exit();
    } else {
        echo "Error: " . mysqli_error($sqlid);
    }

    mysqli_stmt_close($deleteStmt);
}

if (isset($_POST["update"])) {
    $updateId = $_POST["update"];
    $updatedkredit = $_POST["updatedkredit"];
    $updatedfelev = $_POST["updatedfelev"];
    $updatedkurzusnev = $_POST["updatedkurzusnev"];

    $updateQuery = "UPDATE kurzus SET kredit = ?, félév = ?, kurzusnév = ? WHERE kurzuskód = ?";
    $updateStmt = mysqli_prepare($sqlid, $updateQuery);
    mysqli_stmt_bind_param($updateStmt, "issi", $updatedkredit, $updatedfelev, $updatedkurzusnev, $updateId);

    if (mysqli_stmt_execute($updateStmt)) {
        header("Location: kurzusok.php");
        exit();
    } else {
        echo "Error: " . mysqli_error($sqlid);
    }

    mysqli_stmt_close($updateStmt);
}

if (isset($_POST["submit"])) {
    $newkredit = $_POST["newkredit"];
    $newfelev = $_POST["newfelev"];
    $newkurzusnev = $_POST["newkurzusnev"];

    if (mysqli_select_db($sqlid, 'moodle')) {


            // ha nem letezik ilyen kód
            $insertQuery = "INSERT INTO kurzus (kurzuskód, kredit, félév, kurzusnév) VALUES (NULL, ?, ?, ?)";
            $stmt = mysqli_prepare($sqlid, $insertQuery);
            mysqli_stmt_bind_param($stmt, "iis" , $newkredit, $newfelev, $newkurzusnev);

            if (mysqli_stmt_execute($stmt)) {
                $insertsiker = true;
                $indexhiba = false;

                header("Location: kurzusok.php");
                exit();
            } else {
                echo "Error: " . mysqli_error($sqlid);
            }

            mysqli_stmt_close($stmt);
        }
}

mysqli_close($sqlid);
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Kurzusok</title>
    <link rel="stylesheet" href="css.css">
</head>
<body>
<div id="navdiv">
    <nav>
        <?php if(!isset($_SESSION["felhasznalonev"]) || empty($_SESSION["felhasznalonev"])):;?>
            <a href="login.php" class="navlink">Bejelentkezés</a>
            <?php endif; ?>
        <?php if(isset($_SESSION["felhasznalonev"]) && ($_SESSION["szerepkor"] == "admin" || $_SESSION["szerepkor"] == "tanár")):?>
            <a href="kurzusok.php" class="navlink" id="active">Kurzusok</a>
            <a href="naplo.php" class="navlink">Napló</a>
        <?php elseif(isset($_SESSION["felhasznalonev"]) && $_SESSION["szerepkor"] == "diák"):?>
            <a href="kurzusok.php" class="navlink" id="active">Kurzusok</a>
        <?php endif; ?>
    </nav>
</div>

<main>
    <?php
    echo "<table border='1' style='margin: 5% auto; width: 75%'>";
    echo "<tr>";
    echo "<th>kredit</th>";
    echo "<th>félév</th>";
    echo "<th>kurzusnév</th> ";
    echo "<th>tananyagok száma</th> ";
    if ($_SESSION["szerepkor"] === "admin" || $_SESSION["szerepkor"] === "tanár") {
        echo "<th>Műveletek</th>";
    }
    echo "</tr>";

    while ($egySor = mysqli_fetch_assoc($eredmeny)) {
        echo '<tr>';
        echo '<td>' . $egySor["kredit"] . '</td>';
        echo '<td>' . $egySor["félév"] . '</td>';
        echo '<td><a href="tananyagok.php?kkurzuskod=' . urlencode($egySor["kurzuskód"]) . '">' . $egySor["kurzusnév"] . '</a></td>';
        echo '<td>' . $egySor["tananyag_száma"] . '</td>';
        if ($_SESSION["szerepkor"] === "admin" || $_SESSION["szerepkor"] === "tanár") {
            echo '<td><a href="?delete=' . $egySor["kurzuskód"] . '">' . "Törlés" . '</a> <a href="?update=' . $egySor["kurzuskód"] . '">' . "Módosítás" . '</a></td>';
        }
        echo '</tr>';

        if (isset($_GET['update']) && $_GET['update'] == $egySor["kurzuskód"]) {
            echo '<tr>';
            echo '<form action="kurzusok.php" method="post">';
            echo '<input type="hidden" name="update" value="' . $egySor["kurzuskód"] . '">';
            echo '<td></td>';
            echo '<td><input type="text" name="updatedkredit" value=""></td>';
            echo '<td><input type="text" name="updatedfelev" value=""></td>';
            echo '<td><input type="text" name="updatedkurzusnev" value=""></td>';
            echo '<td><button type="submit" name="mentes">Mentés</button></td>';
            echo '</form>';
            echo '</tr>';
        }
    }
    if($_SESSION["szerepkor"] === "admin" || $_SESSION["szerepkor"] === "tanár") {
        echo "<form action='kurzusok.php' enctype='multipart/form-data' method='post'>";
        echo '<tr>';

        echo '<td></td>';
        echo'<td><input type="text" name="newkredit"></td>
            <td><input type="text" name="newfelev"></td>
            <td><input type="text" name="newkurzusnev"></td>';
        echo '<td><button type="submit" name="submit">Hozáadás</button></td>';
        echo '</tr>';
        echo "</form>";
    }
    echo "</table>";

    ?>
</main>

<footer>
    <?php
    if(isset($_SESSION["felhasznalonev"])):?>)
        <p style="text-align: right; font-family: Arial,sans-serif; font-size: 12px;"><a style="color: white" href="kijelentkezes.php">Kijelentkezés</a></p>
    <?php endif; ?>
</footer>
</body>
</html>
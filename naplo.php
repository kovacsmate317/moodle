<?php
session_start();


$sqlid = mysqli_connect('localhost', 'root', '', 'moodle') or die("Hibás csatlakozás!");


mysqli_query($sqlid, "SET character_set_results=utf8");
mysqli_set_charset($sqlid, 'utf8');
if (mysqli_select_db($sqlid, 'moodle')) {
    $lekerd = "SELECT * FROM napló";
    $eredmeny = mysqli_query($sqlid, $lekerd) or die("hiba");
    if(isset($_POST["lekérés"])) {
        $id = $_POST["id"];
        $osszlekerd = "SELECT felhasználó.felhasználónév, napló.mikor, napló.mit
        FROM felhasználó
        INNER JOIN napló ON felhasználó.felhasználónév = napló.felhasználónév
        WHERE napló.`tananyag azonosító` = '$id'";

        $osszeredmeny = mysqli_query($sqlid, $osszlekerd) or die("hiba");
    }
} else {
    echo "hiba";
}

?>
<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Napló</title>
    <link rel="stylesheet" href="css.css">
</head>
<body>
<div id="navdiv">
    <nav>
        <?php if(!isset($_SESSION["felhasznalonev"]) || empty($_SESSION["felhasznalonev"])):?>
            <a href="login.php" class="navlink" id="active">Bejelentkezés</a>
        <?php endif; ?>
        <?php if(isset($_SESSION["felhasznalonev"]) && ($_SESSION["szerepkor"] == "admin" || $_SESSION["szerepkor"] == "tanár")):?>
            <a href="kurzusok.php" class="navlink">Kurzusok</a>
            <a href="naplo.php" class="navlink" id="active">Napló</a>
        <?php endif; ?>
    </nav>
</div>
<main>

    <?php
    echo "<table border='1' style='margin: 5% auto; width: 75%'>
        <tr>
            <th>naplóid</th>
            <th>mit</th>
            <th>felhasználónév</th>
            <th>mikor</th>
            <th>kurzuskód</th>
            <th>tananyag azonosító</th>
            <th>Eltelt idő</th>
           
        </tr>";

    while( $egySor = mysqli_fetch_assoc($eredmeny) ) {
        echo '<tr>';
        echo '<td>' . $egySor["naplóid"] . '</td>';
        echo '<td>' . $egySor["mit"] . '</td>';
        echo '<td>' .$egySor["felhasználónév"].'</td>';
        echo '<td>' . date_format(date_create($egySor["mikor"]), 'Y. m. d. H:i:s') . '</td>';
        echo '<td>' . $egySor["kurzuskód"] . '</td>';
        echo '<td>' . $egySor["tananyag azonosító"] . '</td>';
        if(isset( $egySor["Eltelt idő"])) {
            echo '<td>' . $egySor["Eltelt idő"] . '</td>';
        }
        echo '</tr>';
    }
    ?>
    <form method="POST">
        Specifikus tananyagról szóló bejegyzések lekérése <br>
        <label for="id">Tananyag Azonosító:</label>
        <input type="text" id="id" name="id" required>
        <button type="submit" name="lekérés">Lekérés</button>
    </form>
<?php
    if (isset($_POST["lekérés"])) {

        echo "<table border='1' style='margin: 5% auto; width: 75%'>
            <tr>
                <th>Felhasználónév</th>
                <th>Mikor</th>
                <th>Mit</th>
            </tr>";

        while ($row = mysqli_fetch_assoc($osszeredmeny)) {
            echo '<tr>';
            echo '<td>' . $row["felhasználónév"] . '</td>';
            echo '<td>' . date_format(date_create($row["mikor"]), 'Y. m. d. H:i:s') . '</td>';
            echo '<td>' . $row["mit"] . '</td>';
            echo '</tr>';
        }
    }
    ?>

</main>

<footer>
    <?php
    if(isset($_SESSION["felhasznalonev"])):?>)
        <p style="text-align: right; font-family: Arial,sans-serif; font-size: 12px;"><a style="color: white" href="kijelentkezes.php">Kijelentkezés</a></p>
    <?php endif; ?>
</footer>

</body>
</html>

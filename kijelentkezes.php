<?php
session_start();
$sqlid = mysqli_connect('localhost', 'root', '', 'moodle') or die("Hibás csatlakozás!");


mysqli_query($sqlid, "SET character_set_results=utf8");
mysqli_set_charset($sqlid, 'utf8');

//modositani a databaseban a bejelentkezve

$updateQuery = "UPDATE felhasználó SET bejelentkezve = 0 WHERE felhasználónév = ?";
$updateStmt = mysqli_prepare($sqlid, $updateQuery);
mysqli_stmt_bind_param($updateStmt, "s",  $_SESSION["felhasznalonev"]);
mysqli_stmt_execute($updateStmt);
mysqli_stmt_close($updateStmt);

session_unset(); // $_Session ures lesz
session_destroy();
header("Location: login.php");

?>
<?php
session_start();

    $indexhiba = false;
    $sqlid = mysqli_connect('localhost', 'root', '', 'moodle') or die("Hibás csatlakozás!");
    $kurzusid = 0;
    mysqli_query($sqlid, "SET character_set_results=utf8");
    mysqli_set_charset($sqlid, 'utf8');


    if(isset($_GET["kkurzuskod"])){
        $_SESSION["kurzuskod"] = $_GET["kkurzuskod"];
        $kurzusid = $_GET["kkurzuskod"];
        $_SESSION["kkurzusid"] = $kurzusid;
    }

if (isset($_POST["deleteBtn"])) {
    $deleteId = $_POST["törlés"];
    $deleteQuery = "DELETE FROM tananyag WHERE `tananyag azonosító` = ?";
    $deleteStmt = mysqli_prepare($sqlid, $deleteQuery);
    mysqli_stmt_bind_param($deleteStmt, "i", $deleteId);

    if (!mysqli_stmt_execute($deleteStmt)) {
        echo "Error: " . mysqli_error($sqlid);
    }

    mysqli_stmt_close($deleteStmt);
}

    if (mysqli_select_db($sqlid, 'moodle')) {

        if (isset($_GET["kkurzuskod"])) {
            $kurzusid = $_GET['kkurzuskod'];
        }
        if (isset($_POST["insert"])) {
            $newtananyagnev = $_POST["newtananyagnev"];
            $szoveg = $_POST["newszoveg"];


            $insertQuery = "INSERT INTO `tananyag` (`tananyag azonosító`, `létrehozó`, `tananyag neve`, `létrehozás dátuma`, `kurzuskód`, `szöveg`) VALUES (NULL, ?, ?, CURRENT_TIMESTAMP, ?, ?)";
            $stmt = mysqli_prepare($sqlid, $insertQuery);

            mysqli_stmt_bind_param($stmt, "ssis", $_SESSION["felhasznalonev"], $newtananyagnev,$_SESSION["kurzuskod"] ,$szoveg);

            if (mysqli_stmt_execute($stmt)) {
                $insertsiker = true;
                $indexhiba = false;

            } else {
                echo "Error: " . mysqli_error($sqlid);
            }

            mysqli_stmt_close($stmt);
        }
        $naplolekerd = "SELECT MAX(mikor) AS utolsó_megnyitás, COUNT(*) AS megnyitva_count FROM napló WHERE `tananyag azonosító` = ?";
        $naploStmt = mysqli_prepare($sqlid, $naplolekerd);
        mysqli_stmt_bind_param($naploStmt, "i", $tkurzusid);
        mysqli_stmt_execute($naploStmt);
        $naploEredmeny = mysqli_stmt_get_result($naploStmt);
        $naploSor = mysqli_fetch_assoc($naploEredmeny);

        $lekerd = "SELECT tananyag.*, kurzus.kurzusnév
               FROM tananyag
               INNER JOIN kurzus ON tananyag.kurzuskód = kurzus.kurzuskód
               WHERE tananyag.kurzuskód = ?";
        $stmt = mysqli_prepare($sqlid, $lekerd);
        mysqli_stmt_bind_param($stmt, "i", $kurzusid);
        mysqli_stmt_execute($stmt);
        $eredmeny = mysqli_stmt_get_result($stmt);
    } else {
        echo "hiba";
    }



?>
<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Tananyagok</title>
    <link rel="stylesheet" href="css.css">
</head>
<body>
<div id="navdiv">
    <nav>
        <?php if(!isset($_SESSION["felhasznalonev"]) || empty($_SESSION["felhasznalonev"])):;?>
            <a href="login.php" class="navlink">Bejelentkezés</a>
            <?php header("Location: login.php"); endif; ?>
        <?php if(isset($_SESSION["felhasznalonev"]) && ($_SESSION["szerepkor"] == "admin" || $_SESSION["szerepkor"] == "tanár")):?>
            <a href="kurzusok.php" class="navlink">Kurzusok</a>
            <a href="naplo.php" class="navlink">Napló</a>
        <?php elseif(isset($_SESSION["felhasznalonev"]) && $_SESSION["szerepkor"] == "diák"):?>
            <a href="kurzusok.php" class="navlink">Kurzusok</a>
        <?php endif; ?>
    </nav>
</div>

<main>
    <?php
    echo "<table border='1' style='margin: 5% auto; width: 75%'>
        <tr>
            <th>tananyag <br>azonosító</th>
            <th>létrehozó</th>
            <th>tananyag neve</th>
            <th>létrehozás dátuma</th>
            <th>kurzuskód</th>
            <th>megnyitva</th>
            <th>utolsó megnyitás</th>";
            if($_SESSION["szerepkor"] === "admin" || $_SESSION["szerepkor"] === "tanár"){
            echo "<th>Műveletek</th>";
            }
        echo "</tr>";

    while ($egySor = mysqli_fetch_assoc($eredmeny)) {
        $tkurzusid = $egySor["tananyag azonosító"];

        // Query to get megnyitva data from napló
        $naplolekerd = "SELECT megnyitva, MAX(mikor) AS utolsó_megnyitás FROM napló WHERE `tananyag azonosító` = ?  AND `felhasználónév` = ? GROUP BY megnyitva";
        $naploStmt = mysqli_prepare($sqlid, $naplolekerd);
        mysqli_stmt_bind_param($naploStmt, "is", $tkurzusid, $_SESSION["felhasznalonev"]);
        mysqli_stmt_execute($naploStmt);
        $naploEredmeny = mysqli_stmt_get_result($naploStmt);

        // Fetch the first row (assuming there's only one)
        $naploSor = mysqli_fetch_assoc($naploEredmeny);

        echo '<tr>';
        echo '<td>'. $egySor["tananyag azonosító"] .'</td>';
        $tid = $egySor["tananyag azonosító"];
        echo '<td>'. $egySor["létrehozó"] .'</td>';
        echo '<td><a href="tananyagszoveg.php?tkurzusid=' . urlencode($egySor["tananyag azonosító"]) . '&clicked=true">' . $egySor["tananyag neve"] . '</a></td>';
        echo '<td>'. date_format(date_create($egySor["létrehozás dátuma"]), 'Y. m. d. H:i:s') .'</td>';
        echo '<td>'. $egySor["kurzuskód"] .'</td>';
        if(isset($naploSor["megnyitva"])) {
            echo '<td>' . $naploSor["megnyitva"] . '</td>';
        }else{
            echo '<td>Nem</td>';
        }
        if(isset($naploSor["utolsó_megnyitás"])) {
            echo '<td>' . $naploSor["utolsó_megnyitás"] . '</td>';
        }else{
            echo '<td> </td>';
        }


        mysqli_stmt_close($naploStmt);

        if($_SESSION["szerepkor"] === "admin" || $_SESSION["szerepkor"] === "tanár") {
            echo "<td>
                <form action='tananyagok.php?kkurzuskod=$kurzusid' method='post'>
    <input type='hidden' name='törlés' value='" . $tid . "'>
    <input type='submit' value='törlés' name='deleteBtn'>
</form></td>";
        }
        echo '</tr>';
    }
    if($_SESSION["szerepkor"] === "admin" || $_SESSION["szerepkor"] === "tanár") {
        echo "<form action='tananyagok.php?kkurzuskod=$kurzusid' enctype='multipart/form-data' method='post'>";
        echo '<tr>';
        echo'<td></td><td></td>
            <td><input type="text" name="newtananyagnev" placeholder="tananyag neve"></td>
            <td><input type="text" name="newszoveg" placeholder="tananyag szövege"></td>
            <td></td>
            <td></td>
            <td></td>';
        echo '<td><button type="submit" name="insert">Hozáadás</button></td>';
        echo '</tr>';
        echo "</form>";
    }
    ?>
</main>

<footer>
    <?php
    if(isset($_SESSION["felhasznalonev"])):?>)
        <p style="text-align: right; font-family: Arial,sans-serif; font-size: 12px;"><a style="color: white" href="kijelentkezes.php">Kijelentkezés</a></p>
    <?php endif; mysqli_close($sqlid)?>
</footer>
</body>
</html>
-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2023 at 10:40 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moodle`
--

-- --------------------------------------------------------

--
-- Table structure for table `felhasználó`
--

CREATE TABLE `felhasználó` (
  `felhasználónév` varchar(30) NOT NULL,
  `név` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jelszó` varchar(128) NOT NULL,
  `bejelentkezve` tinyint(1) NOT NULL,
  `szerepkör` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `felhasználó`
--

INSERT INTO `felhasználó` (`felhasználónév`, `név`, `email`, `jelszó`, `bejelentkezve`, `szerepkör`) VALUES
('a', 'a', 'a@a.com', '$2y$10$.99yovAL.GJlhLblBqYCIO0belpmqA5MKRB2YWeMsCEk/rNLUNozm', 0, 'tanár'),
('admin', 'Kovács Máté', 'admin@admin.hu', '$2y$10$IGLjf2lbtuzxl/R6BUUGoOY.OndXgt4BIJ.rKJ8Wl5xLKtl5ewYqi', 0, 'admin'),
('b', 'b', 'b@b.co', '$2y$10$J0XNOZdFoMHFXQja2qLuZuE2ywvKBHg58i3XVTjfYeos4MKTehUuK', 0, 'tanár'),
('diak', 'x y ', 'diak@diak.com', '$2y$10$RXvAkTKSMFtK7L9V6EiaEe3tdoSySDo5kqbirKseQaw65ldV6.HKi', 0, 'diák'),
('diak2', 'diak ketto', 'sda@g.co', '$2y$10$xx7RThrxoHVMRnj6OWME8e16D1T.oKEp2bTQL/9zooRXIZUMAvFkm', 0, 'tanár'),
('igen', 'i gen', 'igen@igen.hu', '$2y$10$LcLT4AOR/7SQKIfA2TNcJ.0A7O92jHqbahwZYeVJRcMJAAtD5buM.', 0, 'diák'),
('k', 'k', 'k@kco', '$2y$10$E2PylzhicQEawL/0TldqzuT.gOC4AOPOuS6ExCrY1W/X5L16HYmru', 0, 'tanár'),
('macska', 'macska', 'macs@ka.com', '$2y$10$TzCq.FuRbVX/CmLm1G6aQO8kGXNUunqhz047TzBZqd9poQA.nH/jq', 0, 'diák'),
('nnn', 'nnn', 'nnn@go.co', '$2y$10$LaYZ7Y/GVEYICxCvbzCsPeZ9VUwlG1D43rAL1iIzIlT6wp5Vf/caC', 0, 'diák'),
('tanar', 'das', 'tanar@tan.hu', '$2y$10$U26gXvQZAIbkB7i3TTfE5ufqMa/ivtQOpR3z9hPOd82jNZcc1ikP.', 0, 'tanár'),
('x', 'x', 'x@x.co', '$2y$10$5WrgSYUmz7p9ruUEty8kWOOBPbkmBqxFXThM9pBwfM/knTqD4B9Li', 0, 'tanár');

-- --------------------------------------------------------

--
-- Table structure for table `kurzus`
--

CREATE TABLE `kurzus` (
  `kurzuskód` int(11) NOT NULL,
  `kredit` int(2) NOT NULL,
  `félév` int(2) DEFAULT NULL,
  `kurzusnév` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `kurzus`
--

INSERT INTO `kurzus` (`kurzuskód`, `kredit`, `félév`, `kurzusnév`) VALUES
(2, 3, 1, 'kalk.'),
(3, 3, 3, 'adatb.'),
(4, 2, 2, 'op. kut.'),
(5, 3, 5, 'dimat.'),
(6, 10, 5, 'x y'),
(7, 100, 100, 'száz'),
(8, 1, 1, 'sz'),
(9, 3, 2, 'meg nem nyitott'),
(10, 3, 3, 'enn'),
(12, 2, 3, 'három');

-- --------------------------------------------------------

--
-- Table structure for table `napló`
--

CREATE TABLE `napló` (
  `naplóid` int(11) NOT NULL,
  `mit` varchar(50) NOT NULL,
  `felhasználónév` varchar(30) NOT NULL,
  `mikor` timestamp NOT NULL DEFAULT current_timestamp(),
  `kurzuskód` int(11) NOT NULL,
  `tananyag azonosító` int(11) NOT NULL,
  `eltelt idő` time DEFAULT NULL,
  `megnyitva` enum('Igen','Nem') NOT NULL DEFAULT 'Nem'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `napló`
--

INSERT INTO `napló` (`naplóid`, `mit`, `felhasználónév`, `mikor`, `kurzuskód`, `tananyag azonosító`, `eltelt idő`, `megnyitva`) VALUES
(30, 'Megnyitotta a 85 azonosítójú tananyagot', 'admin', '2023-11-26 21:32:07', 8, 85, NULL, 'Igen'),
(31, 'Bezárta a 85 azonosítójú tananyagot', 'admin', '2023-11-26 21:32:08', 8, 85, NULL, 'Nem'),
(34, 'Megnyitotta a 83 azonosítójú tananyagot', 'k', '2023-11-26 21:34:22', 6, 83, NULL, 'Igen'),
(35, 'Megnyitotta a 83 azonosítójú tananyagot', 'k', '2023-11-26 21:34:33', 6, 83, NULL, 'Igen'),
(36, 'Megnyitotta a 83 azonosítójú tananyagot', 'k', '2023-11-26 21:34:45', 6, 83, NULL, 'Igen'),
(37, 'Megnyitotta a 83 azonosítójú tananyagot', 'k', '2023-11-26 21:35:35', 6, 83, NULL, 'Igen'),
(38, 'Bezárta a 83 azonosítójú tananyagot', 'k', '2023-11-26 21:35:36', 6, 83, NULL, 'Nem'),
(39, 'Megnyitotta a 84 azonosítójú tananyagot', 'k', '2023-11-26 21:35:43', 7, 84, NULL, 'Igen'),
(40, 'Bezárta a 84 azonosítójú tananyagot', 'k', '2023-11-26 21:35:44', 7, 84, NULL, 'Nem'),
(41, 'Megnyitotta a 76 azonosítójú tananyagot', 'k', '2023-11-26 21:35:47', 2, 76, NULL, 'Igen'),
(42, 'Bezárta a 76 azonosítójú tananyagot', 'k', '2023-11-26 21:35:49', 2, 76, NULL, 'Nem'),
(43, 'Megnyitotta a 85 azonosítójú tananyagot', 'k', '2023-11-26 21:35:55', 8, 85, NULL, 'Igen'),
(44, 'Bezárta a 85 azonosítójú tananyagot', 'k', '2023-11-26 21:35:56', 8, 85, NULL, 'Nem'),
(45, 'Megnyitotta a 76 azonosítójú tananyagot', 'diak', '2023-11-26 21:36:08', 2, 76, NULL, 'Igen'),
(46, 'Bezárta a 76 azonosítójú tananyagot', 'diak', '2023-11-26 21:36:09', 2, 76, NULL, 'Nem'),
(47, 'Megnyitotta a 85 azonosítójú tananyagot', 'diak', '2023-11-26 21:36:12', 8, 85, NULL, 'Igen'),
(48, 'Bezárta a 85 azonosítójú tananyagot', 'diak', '2023-11-26 21:36:13', 8, 85, NULL, 'Nem'),
(49, 'Megnyitotta a 80 azonosítójú tananyagot', 'diak', '2023-11-26 21:36:15', 5, 80, NULL, 'Igen'),
(50, 'Bezárta a 80 azonosítójú tananyagot', 'diak', '2023-11-26 21:36:16', 5, 80, NULL, 'Nem'),
(51, 'Megnyitotta a 84 azonosítójú tananyagot', 'admin', '2023-11-26 21:36:28', 7, 84, NULL, 'Igen'),
(52, 'Bezárta a 84 azonosítójú tananyagot', 'admin', '2023-11-26 21:36:28', 7, 84, NULL, 'Nem'),
(53, 'Megnyitotta a 80 azonosítójú tananyagot', 'admin', '2023-11-26 21:36:31', 5, 80, NULL, 'Igen'),
(54, 'Bezárta a 80 azonosítójú tananyagot', 'admin', '2023-11-26 21:36:32', 5, 80, NULL, 'Nem'),
(55, 'Megnyitotta a 78 azonosítójú tananyagot', 'admin', '2023-11-26 21:36:34', 3, 78, NULL, 'Igen'),
(56, 'Bezárta a 78 azonosítójú tananyagot', 'admin', '2023-11-26 21:36:35', 3, 78, NULL, 'Nem'),
(57, 'Megnyitotta a 88 azonosítójú tananyagot', 'admin', '2023-11-26 21:38:33', 10, 88, NULL, 'Igen'),
(58, 'Bezárta a 88 azonosítójú tananyagot', 'admin', '2023-11-26 21:38:34', 10, 88, NULL, 'Nem');

-- --------------------------------------------------------

--
-- Table structure for table `tananyag`
--

CREATE TABLE `tananyag` (
  `tananyag azonosító` int(11) NOT NULL,
  `létrehozó` varchar(30) NOT NULL,
  `tananyag neve` varchar(30) NOT NULL,
  `létrehozás dátuma` datetime NOT NULL,
  `kurzuskód` int(4) NOT NULL,
  `szöveg` varchar(4000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `tananyag`
--

INSERT INTO `tananyag` (`tananyag azonosító`, `létrehozó`, `tananyag neve`, `létrehozás dátuma`, `kurzuskód`, `szöveg`) VALUES
(76, 'admin', 'deriválás', '2023-11-26 21:52:46', 2, 'növekedésének elemzésével, a függvényvizsgálattal. A deriváltból következtethetünk a függvény  menetére (azaz, hogy monoton növekvő vagy monoton fogyó-e), szélsőértékeire (lehet-e az adott pontban maximuma vagy minimuma), grafikonjának görbületére (konvex vagy konkáv-e a függvénygörbe) a növekedés mértékére (gyorsan változik-e a függvény vagy lassan) a függvény közelítő értékére, lineárissal történő közelíthetőségére. A derivált fogalma a 16. és a 17. században fejlődött ki, geometriai és mechanikai problémák megoldása során. Azóta a differenciálszámítás a matematika nagyon jól feldolgozott témaköre,[1] alkalmazása számos tudományban nélkülözhetetlen. Szigorú matematikai fogalomként csak a függvények differenciálhatóságának fogalmával együtt tárgyalható, de szemléletes tartalma enélkül is megérthető.  Pontos definíció és jelölések Legyen f egyváltozós valós függvény, x0 az értelmezési tartományának egy belső pontja. Ekkor az f függvény x0-beli deriváltján vagy differenciálhányadosán[2] a  lim � → � 0 � ( � ) − � ( � 0 ) � − � 0 {\\displaystyle \\lim \\limits _{x\\to x_{0}}{\\frac {f(x)-f(x_{0})}{x-x_{0}}}} határértéket értjük, ha ez létezik és véges (azaz valós szám).[3]  Mivel a határérték egyértelmű, ha egyáltalán létezik, ugyanígy a derivált is egyértelmű. A fenti határérték, azaz a derivált jele:  � ′ ( � 0 ) {\\displaystyle f\'(x_{0})\\,}, vagy  � � ( � 0 ) � � {\\displaystyle {\\frac {df(x_{0})}{dx}}}, vagy  � � � � | � = � 0 {\\displaystyle \\left.{\\frac {df}{dx}}\\right|_{x=x_{0}}} Az első a Lagrange-féle jelölés, ő használta először a „derivált” kifejezést. A második a Leibniz-féle, ő differenciálhányadosnak nevezte (később Hamilton differenciálkoefficiensként említi). Newton a deriváltat ponttal jelölte:  � ˙{\\displaystyle \\scriptstyle {\\dot {v}}} és fluxiónak nevezte.[4]  Rögzített x esetén az  � ( � ) − � ( � 0 ) � − � 0 {\\displaystyle {\\frac {f(x)-f(x_{0})}{x-x_{0}}}} hányadost differenciahányadosnak vagy különbségi hányadosnak szokás nevezni. Ezután a derivált definiálható úgy is, mint a különbségi hányados  � → � 0 {\\displaystyle x\\to x_{0}} melletti határértéke.  A jobb oldali derivált akkor létezik, ha a  lim � → � 0 + 0 � ( � ) − � ( � 0 ) � − � 0 = lim ℎ → 0 + 0 � ( � 0 + ℎ ) − � ( � 0 ) ℎ {\\displaystyle \\lim \\limits _{x\\to x_{0}+0}{\\frac {f(x)-f(x_{0})}{x-x_{0}}}=\\lim \\limits _{h\\to 0+0}{\\frac {f(x_{0}+h)-f(x_{0})}{h}}} határérték létezik és véges.  A bal oldali derivált akkor létezik, ha a  lim � → � 0 − 0 � ( � ) − � ( � 0 ) � − � 0 = lim ℎ → 0 − 0 � ( � 0 + ℎ ) − � ( � 0 ) ℎ {\\displaystyle \\lim \\limits _{x\\to x_{0}-0}{\\frac {f(x)-f(x_{0})}{x-x_{0}}}=\\lim \\limits _{h\\to 0-0}{\\frac {f(x_{0}+h)-f(x_{0})}{h}}} határérték létezik és véges.  Magyarázat Az x pontbeli differenciálhányados a fenti definícióval ekvivalens módon felírható a következőképpen is:  lim ℎ → 0 � ( � + ℎ ) − � ( � ) ℎ {\\displaystyle \\lim \\limits _{h\\to 0}{\\frac {f(x+h)-f(x)}{h}}} illetve  lim Δ � → 0 � ( � + Δ � ) − � ( � ) Δ � {\\displaystyle \\lim \\limits _{\\Delta x\\to 0}{\\frac {f(x+\\Delta x)-f(x)}{\\Delta x}}} h-t, illetve Δx-et a független változó növekményének, míg f(x+h) – f(x)-et, illetve f(x+Δx) – f(x) -et a függvény vagy a függő változó növekményének nevezzük. Ez az írásmód a következő szemléletes értelmezésekkel kapcsolatos.  Mechanikai értelmezés A vizsgált függvényt egy mozgó test s(t) út-idő összefüggésének tekintve, t időpontra és Δt időtartamra a következőképp írható fel a különbségi hányados:  � ( � + Δ � ) − � ( � ) Δ � {\\displaystyle {\\frac {s(t+\\Delta t)-s(t)}{\\Delta t}}\\,} A számlálóban a megtett út, a nevezőben az út megtételéhez szükséges idő áll, így a hányados a test [t, t + Δt] időintervallumban számított átlagsebességét adja. Ha „egyre kisebb” Δt időtartamokra számoljuk ki ezt a hányadost, például 0,01, 0,001, … másodpercre (a lényeg, hogy 0-hoz tartunk), akkor a hányados értéke egyre kevésbé változik, és egyre inkább csak a t időpontra jellemző sebességadatot, a pillanatnyi sebességet adja:  � ( � ) = lim Δ � → 0 � ( � + Δ � ) '),
(77, 'admin', 'Projektm.', '2023-11-26 21:55:08', 3, 'Specifikáció: Egy tananyagkezelő rendszerben a tanár és adminisztrátor szerepkörű különböző kurzusokat hozhatunk létre, amelyekhez hallgatókat és tanárokat társíthatunk. A kurzuson belül a tanárok tananyagokat tesznek közzé. Ezeket a tananyagokat a hallgatók megnyithatják, és olvashatják. A tananyagok megnyitását, használatát (például lapozás), és bezárását a rendszer naplózza, ezáltal az tanár lekérdezheti, hogy melyik felhasználó mennyi időt töltött egy-egy lecke tanulásával, mikor nyitotta meg, mikor zárta be. Összesítést kaphat arról, hogy a hallgatók összesen mennyi időt töltöttek a tananyaggal. Tárolt adatok (nem feltétlen jelentenek önálló táblákat):  Felhasználók: egyedi felhasználónév/email, jelszó, név, szerepkör, be van-e jelentkezve  Tananyagok: tananyag azonosító, tananyag neve, létrehozás dátuma, ki hozta létre, melyik kurzushoz tartozik  Kurzusok: kurzuskód, kurzusnév, félév, kredit  Napló: ki végzett valamilyen műveletet a tananyaggal, milyen műveletet végzett, mikor végezte a műveletet, tananyag azonosítója, kurzuskód, mennyi idő telt el a megnyitás és becsukás között Relációk az adatok között: A rendszerben több kurzus van. Egy kurzushoz több tananyag tartozhat, de egy tananyag csak egy kurzushoz tartozik. Egy tananyaghoz több megnyitás, lapozás, bezárás tartozhat. Egy tananyagon több felhasználó is végezhet műveletet és egy felhasználó több tananyagon végezhet műveletet, sőt, egy felhasználó különböző időpontban ugyanazzal a tananyaggal is végezhet műveletet. '),
(78, 'admin', 'Bevezetés az SQL nyelvbe', '2023-11-26 21:56:07', 3, 'Adattípusok Adattípusok Megjegyezzük, hogy az egyes adattípusok kulcsszavai egyes adatbáziskezelő rendszerekben különbözhetnek.  Itt csak a leggyakoribbakat említjük.  Karakter alapú adattípusok CHAR(n): n-hosszú karaktersorozat VARCHAR(n): legfeljebb n-hosszú karaktersorozat CLOB: hosszú szöveg (Character Large OBject) Numerikus adattípusok INT(n), INTEGER(n): n számjegyből álló egész szám REAL: lebegőpontos valós szám DECIMAL(n,m): n számjegyből álló szám, amelynek m tizedesjegye van Dátum, idő adattípusok DATE: dátum, év, hónap, nap TIME: idő, óra, perc, másodperc DATETIME: dátum és idő együtt TIMESTAMP: időbélyeg másodperc pontossággal Egyéb adattípusok BLOB: bináris nagy objektum'),
(79, 'admin', 'Adatmanipuláció MySQL-ben', '2023-11-26 21:56:41', 3, 'Adatmanipuláció MySQL-ben Célok Az adatbázissal támogatott rendszereknél nagyon gyakran már meglévő adatbázissal találkozunk, vagy legalábbis csak egyszer kell megvalósítani a táblákat. A fejlesztés túlnyomó részében az adatkezelést kell megvalósítanunk.  Ebben a leckében az adatok beszúrását, módosítását és törlését fogjuk áttekinteni. Megnézzük továbbá azt is, hogy hogyan lehet ezeket a műveleteket PHP-ben és Java-ban megvalósítani.  Tanulási eredmények Tudás Ismeri az SQL nyelv alapjait.  Ismeri az SQL utasítások beágyazási technikájának lehetőségeit a JDBC függvénykönyvtárral Java-ban és PHP nyelven.  Képesség Létrehoz, töröl, módosít, aktualizál táblákat SQL nyelven relációs adatbázisokban. Kezeli relációsémákat, kulcsokat, indexeket. Ismeri az adatáblák aktualizálásának műveleteit SQL nyelven.  Képes Java és PHP nyelvű programokban, JDBC, és PHP függvénykönyvtárak segítségével adatbázis-kezelő funkciót készíteni.  Attitűd Szem előt tartja az SQL utasítások szintaktikai szabályait és az adatvesztés lehetőségeit aktualizáló utasítások során. Az adatvesztést elkerülendő, különös megfontoltsággal készít és hajt végre törlésre és módosításra vonatkozó SQL utasításokat.  Figyelembe veszi a moduláris szoftverfejlesztés során az adatbiztonsági szempontokat (például a forrásfájlokra vonatkozó láthatósági szinteket) valamint az adatbázis-használatának szükséges idejét. Törekszik a program sérülékenységének csökkentésére.  Autonómia-felelősség Önállóan megírja az SQL nyelvű utasításokat relációs adatbázisokban történő szerkezet és tartalmi változtatásokhoz. Az adatvesztést elkerülendő, különös megfontoltsággal készít és hajt végre törlésre és módosításra vonatkozó SQL utasításokat.  Időbeosztás HTML alapok (tanórán kívül előzetes átnézésre javasoljuk): 30 perc  Beszúrás, módosítás, törlés SQL-ben (tanórán vagy tanórán kívül): 10 perc  Űrlapok készítése HTML-ben + példa (tanórán vagy tanórán kívül): 25 perc  Feladatok (tanórán vagy tanórán kívül): 2 * 10 perc  Házi feladat (tanórán kívül): 10 perc  Projektmunka (tanórán kívül): 1-3 óra a programozási rutintól függően  Ellenőrző kérdések (tanórán vagy tanórán kívül): 5 perc'),
(80, 'admin', 'dimat', '2023-11-26 21:56:56', 5, 'dimat'),
(81, 'tanar', 'xy', '2023-11-26 21:57:18', 6, 'xy'),
(83, 'tanar', 'yx', '2023-11-26 21:57:29', 6, 'yx'),
(84, 'tanar', '100', '2023-11-26 21:57:45', 7, 'száz'),
(85, 'tanar', 'sz', '2023-11-26 21:58:14', 8, 'a'),
(87, 'tanar', 'nincs megnyitva', '2023-11-26 21:58:50', 9, 'eddig'),
(88, 'admin', 'ige', '2023-11-26 22:38:29', 10, 'dsadasdasdadsadsadasdasd'),
(91, 'admin', 'ads', '2023-11-26 22:38:55', 10, 'das');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `felhasználó`
--
ALTER TABLE `felhasználó`
  ADD PRIMARY KEY (`felhasználónév`),
  ADD UNIQUE KEY `felhasználónév` (`felhasználónév`) USING BTREE;

--
-- Indexes for table `kurzus`
--
ALTER TABLE `kurzus`
  ADD PRIMARY KEY (`kurzuskód`);

--
-- Indexes for table `napló`
--
ALTER TABLE `napló`
  ADD PRIMARY KEY (`naplóid`),
  ADD KEY `felhasználónév` (`felhasználónév`),
  ADD KEY `kurzuskód` (`kurzuskód`),
  ADD KEY `tananyag azonosító` (`tananyag azonosító`);

--
-- Indexes for table `tananyag`
--
ALTER TABLE `tananyag`
  ADD PRIMARY KEY (`tananyag azonosító`),
  ADD KEY `tananyag_ibfk_1` (`kurzuskód`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kurzus`
--
ALTER TABLE `kurzus`
  MODIFY `kurzuskód` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `napló`
--
ALTER TABLE `napló`
  MODIFY `naplóid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tananyag`
--
ALTER TABLE `tananyag`
  MODIFY `tananyag azonosító` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `napló`
--
ALTER TABLE `napló`
  ADD CONSTRAINT `napló_ibfk_1` FOREIGN KEY (`felhasználónév`) REFERENCES `felhasználó` (`felhasználónév`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `napló_ibfk_2` FOREIGN KEY (`kurzuskód`) REFERENCES `kurzus` (`kurzuskód`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `napló_ibfk_3` FOREIGN KEY (`tananyag azonosító`) REFERENCES `tananyag` (`tananyag azonosító`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tananyag`
--
ALTER TABLE `tananyag`
  ADD CONSTRAINT `tananyag_ibfk_1` FOREIGN KEY (`kurzuskód`) REFERENCES `kurzus` (`kurzuskód`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

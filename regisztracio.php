<?php


session_start();
$_SESSION["foglalt"] = false;
$_SESSION["jelszoegyezes"] = true;
$_SESSION["sikeresreg"] = false;


if(isset($_POST["submit"])) {
    $nev = $_POST['nev'];
    $felhasznalonev = $_POST['felhasznalonev'];
    $jelszo = $_POST['jelszo'];
    $jelszoujra = $_POST['jelszoujra'];
    $email = $_POST['email'];
    $bejelentkezve = 0;
    $szerepkor = $_POST["szerepkör"];


//kapcs
    $sqlid = mysqli_connect('localhost', 'root', '', 'moodle') or die("Hibás csatlakozás!");


    mysqli_query($sqlid, "SET character_set_results=utf8");
    mysqli_set_charset($sqlid, 'utf8');

    if (mysqli_select_db($sqlid, 'moodle')) {
        $lekerd = "SELECT felhasználónév FROM felhasználó";
        $felhasznalonevek = mysqli_query($sqlid, $lekerd) or die("hiba");
        if (mysqli_num_rows($felhasznalonevek) === 0) {
            $szerepkor = "admin";
        }
    } else {
        echo "hiba";
    }

    while ($row = mysqli_fetch_assoc($felhasznalonevek)) {

        if ($row["felhasználónév"] === $felhasznalonev) {
            $_SESSION["foglalt"] = true;
        }
    }

    if($_POST['jelszo'] != $_POST['jelszoujra']){
        $_SESSION["jelszoegyezes"] = false;
    }


    if($_SESSION["jelszoegyezes"] and !$_SESSION["foglalt"]){
        //előkészítés
        $stmt = mysqli_prepare( $sqlid,"INSERT INTO felhasználó(felhasználónév, email, név, jelszó, bejelentkezve, szerepkör) VALUES (?, ?, ?, ?, ?, ?)");
        //bekötés
        $jelszohash = password_hash($jelszo, PASSWORD_DEFAULT);
        mysqli_stmt_bind_param($stmt, "ssssis", $felhasznalonev, $email,$nev,$jelszohash,$bejelentkezve,$szerepkor);

        $sikeres = mysqli_stmt_execute($stmt);

        if(!$sikeres){
            echo "hiba";
        }
        else{

            $_SESSION["sikeresreg"] = true;
        }


    }
    mysqli_close($sqlid);
}
?>
<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Regisztráció</title>
    <link rel="stylesheet" href="css.css">
</head>
<body>
<div id="navdiv">
    <nav>
        <?php if(!isset($_SESSION["felhasznalonev"]) || empty($_SESSION["felhasznalonev"])):;?>
            <a href="regisztracio.php" class="navlink" id =active>Bejelentkezés</a>
            <?php endif; ?>
        <?php if(isset($_SESSION["felhasznalonev"])):
            header("Location: kurzusok.php");
            ?>
        <?php endif; ?>
    </nav>
</div>

<main>
    <div id="formdiv">
        <h1>Regisztráció</h1>
        <form action="regisztracio.php" method="POST" target="myiframe" autocomplete="off" enctype="multipart/form-data">
            <fieldset id="fieldsetblog">
                <legend style="text-align: center;">Személyes adatok</legend>
                <label>Teljes név
                    <input  type="text" class="inputwidth " name="nev" maxlength="40" placeholder="Nagy Pista" required>
                </label>
                <label>Felhasználónév
                    <input type="text" class="inputwidth " name="felhasznalonev" maxlength="30" placeholder="ngypisti10"  required>
                </label>
                <label>Jelszó
                    <input type="password" class="inputwidth " name="jelszo" maxlength="45" required>
                </label>
                <label>Jelszó újra
                    <input type="password" class="inputwidth " name="jelszoujra" maxlength="45" required>
                </label>
                <label>E-mail
                    <input type="email" class="inputwidth " name="email" maxlength="60" required placeholder="valaki@gmail.com">
                </label>
                <select name="szerepkör">
                    <option value="diák" name="diák">diák</option>
                    <option value="tanár" name="tanár">tanár</option>
                </select>

            </fieldset>
            <fieldset id="gombfield">
                <input type="submit" name="submit" value="Küldés" class="formgombok">
                <a href="login.php" id="vanmarfiokod">Van már fiókod?</a>
            </fieldset>
            <p style="margin-left: 55px">
            <?php
            if($_SESSION["sikeresreg"]) echo "Sikeres regisztráció";
            if($_SESSION["foglalt"]) echo "foglalt username<br>";
            if(!$_SESSION["jelszoegyezes"]) echo "nem ugyanaz a két jelszó\n";
            ?>
            </p>
        </form>
    </div>
</main>
<footer>
    <p style="text-align: center; font-family: Arial,sans-serif; font-size: 12px; height: 10px"></p>
</footer>

</body>
</html>